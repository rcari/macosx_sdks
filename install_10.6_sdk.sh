#!/bin/sh
installer -pkg MacOSX10.6.pkg -target /
# Create symbolic link for darwin11 i686
ln -s /SDKs/MacOSX10.6.sdk/usr/include/c++/4.2.1/i686-apple-darwin10 /SDKs/MacOSX10.6.sdk/usr/include/c++/4.2.1/i686-apple-darwin11
# Create symbolic link for darwin11 x86_64
ln -s /SDKs/MacOSX10.6.sdk/usr/include/c++/4.2.1/x86_64-apple-darwin10 /SDKs/MacOSX10.6.sdk/usr/include/c++/4.2.1/x86_64-apple-darwin11
# Create symbolic link for stdint.h
ln -s /SDKs/MacOSX10.6.sdk/usr/include/gcc/darwin/default/stdint.h /SDKs/MacOSX10.6.sdk/usr/include/stdint.h
echo "If everything ran correctly, the Mac OS 10.6 SDK is installed in /SDKs"
